from django import forms


class CartAddProductForm(forms.Form):
    quantity = forms.IntegerField(max_value=1)
