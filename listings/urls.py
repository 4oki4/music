from django.urls import path

from .views import products_list, product_detail

app_name = 'listings'

urlpatterns = [
    path('', products_list, name='product_list'),
    path('<slug:category_slug>', products_list, name='product_list_by_category'),
    path('<slug:category_slug>/<slug:product_slug>', product_detail, name='product_detail'),
]
