from django.shortcuts import render
from django.shortcuts import get_object_or_404, redirect
from .forms import ReviewForm

from .models import Category, Products, Review


categories = Category.objects.all()


def products_list(request, category_slug=None):
    if category_slug:
        request_category = get_object_or_404(Category, slug=category_slug)
        products = Products.objects.filter(category=request_category)
    else:
        request_category = None
        products = Products.objects.all()

    return render(request, 'product/list.html',
                  {'categories': categories,
                   'request_category': request_category,
                   'products': products})


def product_detail(request, category_slug, product_slug):
    category = get_object_or_404(Category, slug=category_slug)
    product = get_object_or_404(Products, category_id=category.id, slug=product_slug)

    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            cf = review_form.cleaned_data
            author = '4oki4'
            Review.objects.create(product=product, author=author, text=cf['text'], rating=cf['rating'])
        return redirect('listings:product_detail', category_slug=category_slug, product_slug=product_slug)
    else:
        review_form = ReviewForm()

    return render(request, 'product/detail.html', {'product': product, 'review_form': review_form})

