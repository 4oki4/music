from django.contrib import admin

from .models import Category, Products, Review


#admin.site.register(Category)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


class OrderReviewInLine(admin.TabularInline):
    model = Review


@admin.register(Products)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'slug', 'price', 'available',)
    list_filter = ('category', 'available',)
    list_editable = ('price', 'available',)
    prepopulated_fields = {'slug': ('name',)}

    inlines = [OrderReviewInLine]
